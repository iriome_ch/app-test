<?php

namespace App\Imports;

use App\Models\Customer;
use Maatwebsite\Excel\Concerns\ToModel;

class CustomersImport implements ToModel
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        // return new Customer([
        //     'name' => $row['name'],
        //     'email' => $row['email'],
        //     'address' => $row['address'],
        //     'phone' => $row['phone'],
        // ]);
        return new Customer([
            'name' => $row[1],
            'email' => $row[2],
            'address' => $row[4],
            'phone' => $row[3],
        ]);
    }
}
