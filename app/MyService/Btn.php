<?php

namespace App\MyService;

class Btn
{
    public function a($url, $text= null)
    {
        // Retorna una etiqueta <a> con el atributo href y el texto pasado por parámetro
        return view('btn.a', ['url' => url($url), 'text' => is_null($text) ? $url : $text ]);
    }

    public function button($text= null, $class= null, $onclick= null)
    {
        // Retorna una etiqueta <button> con el atributo class y el texto pasado por parámetro
        return view('btn.button', ['text' => is_null($text) ? 'Button' : $text, 'class' => is_null($class) ? 'btn-primary' : $class, 'onclick' => is_null($onclick) ? '' : $onclick ]);
    }
}