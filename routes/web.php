<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\PDFController;
// use Illuminate\Support\Facades\App;
use App\MyService\Btn;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes(['verify' => true]);

Route::group(['middleware' => ['verified']], function () {
    Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

    Route::resource('customers', App\Http\Controllers\CustomerController::class);

    Route::get('/customers/{id}/pdf', [App\Http\Controllers\CustomerController::class, 'pdf'])->name('customers.pdf');

    Route::get('/customers-export', [App\Http\Controllers\CustomerController::class, 'export'])->name('export');
    Route::post('/customers-import', [App\Http\Controllers\CustomerController::class, 'import'])->name('import');

    Route::get('generate-pdf', [PDFController::class, 'generatePDF']);
});
